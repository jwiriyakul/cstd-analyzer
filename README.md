# CSTD Analyzer

CSTD Analyzer is the library for measuring technical debt on the code smells.  

### *It will be available after the work has been published.

## Supported Technical Debt Metrics
* Technical Debt Principal
* Paid Technical Debt Interest
* Paid Technical Debt Pricipal
* Paid Techincal Debt
* Cumulative Paid Technical Debt Principal
* Cumulative Paid Technical Debt Interest
* Cumulative Paid Technical Debt

## Supported Code Smells
* Duplicated Code
* Long Method
* God Class (Large Class)
* Feaure Envy

## Supported Effort Estimation Models
* COstructive COst MOdel II (COCOMO II)[<sup>[1]</sup>](#Boehm-2000)
* Change Acceptance Analysis (CAA)[<sup>[2]</sup>](#Kama-2013)

## Library Guide
* The library is a maven project and developed on Eclipse IDE. Check out the soucecode and import as the Eclipse project or import the maven project for other IDEs. 
* In simple usage, a JsonStoredCstdAnalyzer class that store the data in JSON files is provided. the example usage can be found in the cstd-analyzer-example project.
* The library provides the support functionalities  measuring TD on code smells:
    * Parsing the code smell detection results.
    * Analyzing the TD-related activities.
    * Selectable effort estimation models TD measurement.

## COCOMO II drivers rating
This work adapt the effort estimation models that are all based on COCOMO II, and they require the evaluation of the scale factors and effort multipliers rating. Our rated scores of the 8 studied projects in this work are listed in the following table. 

### The COCOMO II Driver scores
|Driver|SF/EM|antlr|argouml|freecol|hibernate|jstock|jung|junit|weka|
|----|----|----|----|----|----|----|----|----|----|
|PREC|SF<sub>1</sub> |2.48|2.48|2.48|2.48|2.48|2.48|2.48|2.48|
|FLEX|SF<sub>2</sub> |0|2.03|0|2.03|2.03|2.03|2.03|2.03|
|RESL|SF<sub>3</sub> |4.24|4.24|4.24|4.24|4.24|4.24|4.24|4.24|
|TEAM|SF<sub>4</sub> |2.19|2.19|2.19|2.19|2.19|2.19|2.19|2.19|
|PMAT|SF<sub>5</sub> |4.68|4.68|4.68|4.68|4.68|4.68|4.68|4.68|
|RELY|EM<sub>1</sub> |1|1.1|1.1|1|0.99|1|1|1.1|
|DATA|EM<sub>2</sub> |1|1|1|1|1|1|1|1|
|CPLX|EM<sub>3</sub> |1|1|1|1|1|1|1|1|
|DOCU|EM<sub>4</sub> |1.23|1|1|1.23|1|1|1.23|1|
|TIME|EM<sub>5</sub> |1|1|1|1|1|1|1|1|
|STOR|EM<sub>6</sub> |1|1|1|1|1|1|1|1|
|PVOL|EM<sub>7</sub> |0.87|0.87|0.87|0.87|0.87|0.87|0.87|0.87|
|ACAP|EM<sub>8</sub> |0.71|0.71|0.71|0.71|0.71|0.71|0.71|0.71|
|PCAP|EM<sub>9</sub>|0.76|0.76|0.76|0.76|0.76|0.76|0.76|0.76|
|PCON|EM<sub>10</sub>|0.81|0.81|0.81|0.81|0.81|0.81|0.81|0.81|
|APEX|EM<sub>11</sub>|0.81|0.81|0.81|0.81|0.81|0.81|0.81|0.81|
|PLEX|EM<sub>12</sub>|0.85|0.85|0.85|0.85|0.85|0.85|0.85|0.85|
|LTEX|EM<sub>13</sub>|0.84|0.84|0.84|0.84|0.84|0.84|0.84|0.84|
|TOOL|EM<sub>14</sub>|0.78|0.78|0.78|0.78|0.78|0.78|0.78|0.78|
|SITE|EM<sub>15</sub>|1|1|1|1|1|1|1|1|

The rated scores rely on the researcher's perception of the projects data in the Qualitas Corpus dataset[<sup>[3]</sup>](#Tempero-2023). Because the public access information of these projects does not reveal all data related to COCOMO drivers, the nominal score is assigned to the undisclosed drivers, i.e., RESL, PMAT, DATA, CPLX, TIME, and STOR. The rated drivers and the evaluation note are in the following table.

### The COCOMO II Driver rating
|Driver|SF/EM|antlr|argouml|freecol|hibernate|jstock|jung|junit|weka|Note|
|----|----|----|----|----|----|----|----|----|----|----|
|PREC|SF<sub>1</sub> |H|H|H|H|H|H|H|H|The projects in data set are not start from the intial version. The developers have worked for a while, thus, the precedentedness is high.|
|FLEX|SF<sub>2</sub> |XH|H|XH|H|H|H|H|H|The projects require integration with other software will get low rating.|
|RESL|SF<sub>3</sub> |N|N|N|N|N|N|N|N|The undisclosed driver.|
|TEAM|SF<sub>4</sub> |H|H|H|H|H|H|H|H|The open source model has strong process for handling the developer contributions.|
|PMAT|SF<sub>5</sub> |N|N|N|N|N|N|N|N|The undisclosed driver.|
|RELY|EM<sub>1</sub> |N|L|L|N|H|N|N|L|L for application type projects, N for library type projects, H for Financial-related projects.|
|DATA|EM<sub>2</sub> |N|N|N|N|N|N|N|N|The undisclosed driver.|
|CPLX|EM<sub>3</sub> |N|N|N|N|N|N|N|N|The undisclosed driver.|
|DOCU|EM<sub>4</sub> |VH|N|N|VH|N|N|VH|N|VH for the library that require documentation, N for the otherwise.|
|TIME|EM<sub>5</sub> |N|N|N|N|N|N|N|N|The undisclosed driver.|
|STOR|EM<sub>6</sub> |N|N|N|N|N|N|N|N|The undisclosed driver.|
|PVOL|EM<sub>7</sub> |L|L|L|L|L|L|L|L|The major change of the projects are all over 12 months.|
|ACAP|EM<sub>8</sub> |VH|VH|VH|VH|VH|VH|VH|VH|Maximum rating with the assumption that deveopers are expert in thier projects.|
|PCAP|EM<sub>9</sub>|VH|VH|VH|VH|VH|VH|VH|VH|Maximum rating with the assumption that deveopers are expert in thier projects.|
|PCON|EM<sub>10</sub>|VH|VH|VH|VH|VH|VH|VH|VH|Maximum rating with the assumption that deveopers are expert in thier projects.|
|APEX|EM<sub>11</sub>|VH|VH|VH|VH|VH|VH|VH|VH|Maximum rating with the assumption that deveopers are expert in thier projects.|
|PLEX|EM<sub>12</sub>|VH|VH|VH|VH|VH|VH|VH|VH|Maximum rating with the assumption that deveopers are expert in thier projects.|
|LTEX|EM<sub>13</sub>|VH|VH|VH|VH|VH|VH|VH|VH|Maximum rating with the assumption that deveopers are expert in thier projects.|
|TOOL|EM<sub>14</sub>|VH|VH|VH|VH|VH|VH|VH|VH|Maximum rating with the assumption that deveopers are expert in thier projects.|
|SITE|EM<sub>15</sub>|N|N|N|N|N|N|N|N|The open source developers work remotely and normally communication by email.|

## References
1. <a name="Boehm-2000">B. Boehm et al., COCOMO II Model Definition Manual. Center for Software Engineering, USC, 2000, p. 90.</a>
2. <a name="Kama-2013">M. N. Kama, “Extending change impact analysis approach for change effort estimation in the software development phase,” in Recent Advances in Computer Engineering Series, 2013.</a>
3. <a name="Tempero-2023">E. Tempero. “Qualitas Corpus.” [Online]. Available: http://qualitascorpus.com/, Accessed on: Jul. 1, 2023.</a>